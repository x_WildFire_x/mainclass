import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import static java.lang.System.getenv;
import static java.lang.System.setOut;

class Env {

    Env() {
        System.out.println("Thits is constructor of class Env");
    }

    public Iterator<String> get_env() {

        Iterator<String> iter = getenv().keySet().iterator();
        return (Iterator<String>) iter;
    }

    public Iterator<Map.Entry<String,String>> get_env_all() {
        Iterator<Map.Entry<String, String>> iter2 = getenv().entrySet().iterator();
        return (Iterator<Map.Entry<String, String>>) iter2;
    }

    public String print_env(Iterator<Map.Entry<String, String>> input){
        while (input.hasNext()) {             
            Map.Entry<String, String> arrayItem = input.next();
            System.out.println("arrayName  " + arrayItem);

        }
        System.out.println(input);
        return null;
    }


}

public class MainClass {

    public static void main(String[] args) {

        Env e = new Env();
        Iterator<Map.Entry<String,String>> a = e.get_env_all();
        System.out.println("This is iterator" + e.get_env_all());
        System.out.println("Print env:");
        System.out.println(e.print_env(a));
    }

}